﻿using System.Collections.Generic;
using UnityEngine;
using Models;

public class QuestionController : MonoBehaviour
{
	private static List<QuestionModel> questions = new List<QuestionModel>();
	private static List<AnswerModel> choices = new List<AnswerModel>();

	private static int INDEX = -1;

	//define question
	private static  string[] questionList = 
	{
		"Is the main circuit of your computer and is also known as the main board",
		"They are big computer systems sensitive to temperature and it has a large storage capacity",
		"It has a less memory and storage capacity than mainframe computers",
		"WWW stands for",
		"Which of the following software is used to view pages",
		"Which of the following is not purely output device",
		"A collection of related instructions organized for a common purpose is reffered to as",
		"On which aspect the analog computers are better than digital",
		"Which operation is not performed by computer",
		"Which of the following is internal memory",
		"Which is not a computer classification",
		"Which language is directly understood by the computer without translation program",
		"Which of the following is used for manufacturing chips",
		"The word length of a computer is measured in",
		"The first general purpose electronic digital computer in the world was",
		"Which of the following is a storage device",
		"A computer program that converts an entire program into machine language is called a/an",
		"What type of device is computer keyboard",
		"Which of the following is not a form of data",
		"Binary circuit elements have"

	};

	//then define choices list
	private static string[,] choicesList = 
	{
		{"Motherboard","Microprocessor","CPU","Keyboard"},
		{"Super Computers","Mini Computers","Mainframe Computers","Micro Cpmputers"},
		{"Micro Computers","Mini Computers","Microprocessor","Super Computers"},
		{"World Web Wide","Web World Wide","World Word Web","World Wide Web"},
		{"Web Browser","Internet Browser","Page Browser","None of the above"},
		{"Speaker","Printer","Screen","Plotter"},
		{"Program","Database","File","All of  the above"},
		{"Speed","Accuracy","Reliability","Automatic"},
		{"Inputting","Processing","Controlling","Understanding"},
		{"Disk","Pen Drives","RAM","CD'S"},
		{"Mainframe","Maxframe","Mini","Notebook"},
		{"Machine Language","Assembly Language","High Level Language","None of the Above"},
		{"Control bus unit","Control unit","Parity Unit","Semiconductor"},
		{"Bytes","Millimeters","C. Meters","Bits"},
		{"UNIVAC","EDVAC","C. ENIAC","All of the Above"},
		{"Tape","Hard Disk","Floppy Disk","Trade Disk"},
		{"Interpreter", "Simulator","Compiler","Commander"},
		{"Memory","Output","Storage","Input"},
		{"Numbers and Characters","Images","Sound","None of the Above"},
		{"One stable state","Two stable state","Three stable state","Four stable state"}
	};

	//finally define answer for each choices
	private static string[] answers = 
	{
		"Motherboard",
		"Super Computers",
		"Micro Computers",
		"World Web Wide",
		"Web Browser",
		"Speaker",
		"Program",
		"Speed",
		"Inputting",
		"Disk",
		"Mainframe",
		"Machine Language",
		"Control bus unit",
		"Bytes",
		"UNIVAC",
		"Tape",
		"Interpreter",
		"Memory",
		"Numbers and Characters",
		"One stable state"
	};

	// Use this for initialization


	//ako na bahala mag lagay ng mga question, choices at tamang sagot sa list 
	public QuestionController()
	{
		for(int ind = 0; ind < questionList.Length; ind++ ){
			questions.Add (new QuestionModel(questionList[ind]));
			choices.Add (
				new AnswerModel(choicesList[ind,0],
					choicesList[ind,1],
					choicesList[ind,2],
					choicesList[ind,3],
					answers[ind]
				)
			);
		}
	}

	public static void InitQuestions(){
		for(int ind = 0; ind < questionList.Length; ind++ ){
			questions.Add (new QuestionModel(questionList[ind]));
			choices.Add (
				new AnswerModel(choicesList[ind,0],
					choicesList[ind,1],
					choicesList[ind,2],
					choicesList[ind,3],
					answers[ind]
				)
			);
		}
	}

//	public static void getCount(){
//		Debug.Log ("Question Count:" + questions.Count);
//		Debug.Log ("Choices Count:" + choices.Count);
//	}

	//generate question based on random index
	public static string generateQuestion()
	{
		int idx = Random.Range (0, questions.Count - 1);

		setQuestionIndex (idx);
		return questions[idx].getQuestion(); 
	}


	//generate answer based on random index made by generateQuestion method()
	// di bali string array ang balik nito sayo for a,b,c,d
	public static string[] generateAnswer(int questionIndex)
	{	
		return shuffleChoices(choices[questionIndex].getChoices());
	}

	private static string[] shuffleChoices(string[] choices)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < choices.Length; t++ )
		{
			string tmp = choices[t];
			int r = Random.Range(t, choices.Length);
			choices[t] = choices[r];
			choices[r] = tmp;
		}
		return choices;
	}

	//nothing much here kasi for declaring lang naman ng question index to
	private static void setQuestionIndex(int questionIndx)
	{
		INDEX = questionIndx;
	}

	//same as above kaso get naman siya, eto yung gagamitin nyo para kuhain yung random index kanina
	public static int getQuestionIndex()
	{
		return INDEX;
	}


	//check answer, padala mo yung choice mo yung mismong word tapos yung questionIndex, gamit ka ng getQuestionIndex()
	// para don tapos return niyan true if tama
	// false kapag mali, then buburahin niya na yung question at choice sa list
	public static bool checkAnswer(string choice,int questionIndex)
	{
		bool answer = false;
		if (choices [questionIndex].getCorrectAnswer () == choice) 
		{
			answer = true;
		}

		removeQuestionAndAnswer ();
		return answer;
	}

	//remove lang yung question and answer sa list tapos set index to its base
	private static void removeQuestionAndAnswer()
	{
		questions.RemoveAt (INDEX);
		choices.RemoveAt (INDEX);
		INDEX = -1;
	}
}


