﻿using System;

namespace Models
{
	public class QuestionModel
	{
		private string question;

		public QuestionModel()
		{

		}

		public QuestionModel(String question)
		{
			this.question = question;
		}

		public string getQuestion()
		{
			return question;
		}


		public void setQuestions(string question)
		{
			this.question = question;
		}
	}

}
