﻿using UnityEngine;
using System.Collections;

public class CharacterProperty{
	
	public static int HEALTH = 10;

	private static int remHealth = 10;

	private static int DAMAGE = 2;



	public static void setHealth(int health){
		remHealth = health;
	}

	public static int getHealth(){
		return remHealth;
	}

	public static void setDamage(int damage){
		DAMAGE = damage;
	}

	public static int getDamage(){
		return DAMAGE;
	}
}
