﻿using UnityEngine;
using System.Collections;

public class EnemyProperty{
	
	private int HEALTH;
	private int remHealth;
	private int DAMAGE;

	public EnemyProperty(){
	}

	public EnemyProperty(int HEALTH,int DAMAGE){
		this.HEALTH = HEALTH;
		remHealth = HEALTH;
		this.DAMAGE = DAMAGE;
	}

	public int getHealth(){
		return this.HEALTH;
	}

	public void setRemHealth(int remHealth){
		this.remHealth = remHealth;
	}

	public int getRemHealth(){
		return remHealth;
	}

	public void setDamage(int damage){
		this.DAMAGE = damage;
	}

	public int getDamage(){
		return this.DAMAGE;
	}
}
