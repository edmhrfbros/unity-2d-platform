﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BattleController : MonoBehaviour {
	public Text[] myChoices;
	public Text questionText;
	public Image playerHealth;
	public Image enemyHealth;

	public Text playerDamage;
	public Text enemyDamage;

	private EnemyProperty enemy;
	// Use this for initialization
	void Start () {
		QuestionController.InitQuestions ();
		setQuestion ();

		enemy = new EnemyProperty (3,1);

		playerHealth.GetComponent<RectTransform>().localPosition = new Vector3(computeHealth(CharacterProperty.getHealth(),CharacterProperty.HEALTH),0f,0f);
		enemyHealth.transform.localPosition = new Vector3 (computeHealth(enemy.getRemHealth(),enemy.getHealth()) * -1,0f,0f);
	}



	public void checkAnswer(Text btnText){
		if(QuestionController.checkAnswer(btnText.text,QuestionController.getQuestionIndex())){
			int currentEnemyHealth = 0;
			if ((enemy.getRemHealth () - CharacterProperty.getDamage ()) > 0) {
				currentEnemyHealth = enemy.getRemHealth () - CharacterProperty.getDamage ();
			}
			enemy.setRemHealth(currentEnemyHealth);
			enemyHealth.transform.localPosition = new Vector3 (computeHealth(enemy.getRemHealth(),enemy.getHealth()) * -1,0f,0f);
			playerDamage.text = "-" + CharacterProperty.getDamage ();
			playerDamage.GetComponent<Animator> ().Play ("DamageAnimation");
			Debug.Log ("ENEMY: " + enemy.getRemHealth());
		}else{
			int currentHealth = 0;
			if ((CharacterProperty.getHealth () - enemy.getDamage ()) > 0) {
				currentHealth = CharacterProperty.getHealth () - enemy.getDamage ();
			}
			CharacterProperty.setHealth (currentHealth);
			playerHealth.GetComponent<RectTransform>().localPosition = new Vector3(computeHealth(CharacterProperty.getHealth (),CharacterProperty.HEALTH),0f,0f);
			enemyDamage.text = "-" + enemy.getDamage ();
			enemyDamage.GetComponent<Animator> ().Play ("DamageAnimation");
			Debug.Log ("CHARACTER: " + CharacterProperty.getHealth());
		}
		setQuestion ();
	}

	public float computeHealth(int RemHealth, int MaxHealth){
		float remHealth = RemHealth;
		float maxHealth = MaxHealth;
		return ((remHealth / maxHealth) * 100.0f) - 100.0f;
	}


	public void setQuestion(){
		questionText.text = QuestionController.generateQuestion ();
		string[] choices = QuestionController.generateAnswer (QuestionController.getQuestionIndex ()); 
		for (int i = 0; i < choices.Length; i++) {
			myChoices [i].text = choices [i];
		}
	}

}
