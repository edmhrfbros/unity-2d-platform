﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class ScreenSwitch : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy") {
			
			col.gameObject.GetComponent<BoxCollider2D> ().isTrigger = true;
			SceneManager.LoadScene ("Battle Scene");

		}
	}
}
	