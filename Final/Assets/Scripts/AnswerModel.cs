﻿using System;

namespace Models

{
	public class AnswerModel

	{
		private string a;
		private string b;
		private string c;
		private string d;

		//		private string correctAnswerLetter;
		private string correctAnswer;

		public AnswerModel(){

		}

		public AnswerModel(string a, string b, string c, string d){
			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
		}

		public AnswerModel(string a,string b, string c, string d, /*string correctAnswerLetter,*/ string correctAnswer){
			this.a = a;
			this.b = b;
			this.c = c;
			this.d = d;
			//			this.correctAnswerLetter = correctAnswerLetter;
			this.correctAnswer = correctAnswer;
		}

		//		private void setCorrentAnswerLetter(String letter){
		//			this.correctAnswerLetter = letter;
		//		}

		private void setCorrectAnswer(String answer){
			this.correctAnswer = answer;
		}

		public void setA(string a){
			this.a = a;
		}
		public void setB(string b){
			this.b = b;
		}
		public void setC(string c){
			this.c = c;
		}
		public void setD(string d){
			this.d = d;
		}

		public string getA(){
			return this.a;
		}

		public string getB(){
			return this.b;
		}

		public string getC(){
			return this.c;
		}

		public string getD(){
			return this.d;
		}

		//		public string getCorrectAnswerLetter(){
		//			return this.correctAnswerLetter;
		//		}

		public string getCorrectAnswer(){
			return this.correctAnswer;	
		}

		public string[] getChoices(){
			return new String[]{this.a,this.b,this.c,this.d};
		}
	}
}

